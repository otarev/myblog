<%-- 
    Document   : article
    Created on : 09.12.2021, 9:59:54
    Author     : Chebotarev M
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

        <div id="main">
            <aside class="leftAside">
                <h2>Каталог</h2>
                <ul>
                    <li><a href="#">Наличие медицинских препаратов</a></li>
                    <li><a href="#">Доставка</a></li>
                    <li><a href="#">О нас</a></li>
                    <li><a href="#">Сертификация</a></li>      
                </ul>
            </aside>
            <section>
                <article>
                    <h1>Тема</h1>
                    <div class="text-article">
                        Текст статьи
                    </div>
                    <div class="fotter-article">
                        <span class="autor">Автор: <a href="#">Главный фармацпт</a></span>
                        <span class="date-article">Дата статьи: 9.12.2021</span>
                    </div>
                </article>
            </section>
        </div>
